import 'package:flutter/material.dart';
import './loginControl.dart';
import './homePage.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  final routes = <String, WidgetBuilder>{
    LoginPage.tag: (context)=>LoginPage(),
    homePage.tag: (context)=>homePage(),
  };

  @override
  Widget build(BuildContext context) {
   return MaterialApp(
     title: 'HELLO WORLD',
     theme: ThemeData(
       primarySwatch: Colors.lightBlue,
     ),
     debugShowCheckedModeBanner: false,
     home: LoginPage(),
     routes: routes,

   );
  }
}
